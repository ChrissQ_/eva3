<%-- 
    Document   : index
    Created on : 08-05-2020, 06:36:01
    Author     : Astaroth
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>API-REST!</title>
    </head>
    <body>
        <h1>CRUD API-REST!</h1>
        <table class="table">
                
                <th scope="col">Nombre</th>Cristian Quintana
                <th scope="col">Profesor</th>Cesar Cruces
                
                <th scope="col">Operaciones CRUD:</th>
                
                
                <th scope="col">Lisar Todos los Registros</th>(Verbose "GET") http://localhost:8080/eva3-1/api/matriculas/
                
                <th scope="col">Listar Un registro en específico</th>(Verbose "GET") http://localhost:8080/eva3-1/api/matriculas/2
                <th scope="col">Agregar Nuevo Registro</th>(Verbose "POST") http://localhost:8080/eva3-1/api/matriculas 
                 /*   {
                    "codAlumno": "382145A",
                    "codCurso": "QU514",
                    "nomAlumno": "Romeo Rojas",
                    "nomProfesor": "Victor Mondaca",
                    "nroMatricula": "3"
                    }*/
                    --Se toma en consideración que el ingreso debe hacerse mediante un archivo en formato JSon.
                    
                <th scope="col">Editar un Registro</th>(Verbose "PUT") http://localhost:8080/eva3-1/api/matriculas
                <th scope="col">Eliminar Un Registro</th>(Verbose "DELETE") http://localhost:8080/eva3-1/api/matriculas/2
            </table>
        
    </body>
</html>
