/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Astaroth
 */
@Entity
@Table(name = "matriculas")
@NamedQueries({
    @NamedQuery(name = "Matriculas.findAll", query = "SELECT m FROM Matriculas m"),
    @NamedQuery(name = "Matriculas.findByNroMatricula", query = "SELECT m FROM Matriculas m WHERE m.nroMatricula = :nroMatricula"),
    @NamedQuery(name = "Matriculas.findByCodAlumno", query = "SELECT m FROM Matriculas m WHERE m.codAlumno = :codAlumno"),
    @NamedQuery(name = "Matriculas.findByNomAlumno", query = "SELECT m FROM Matriculas m WHERE m.nomAlumno = :nomAlumno"),
    @NamedQuery(name = "Matriculas.findByCodCurso", query = "SELECT m FROM Matriculas m WHERE m.codCurso = :codCurso"),
    @NamedQuery(name = "Matriculas.findByNomProfesor", query = "SELECT m FROM Matriculas m WHERE m.nomProfesor = :nomProfesor")})
public class Matriculas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nroMatricula")
    private String nroMatricula;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "codAlumno")
    private String codAlumno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nomAlumno")
    private String nomAlumno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "codCurso")
    private String codCurso;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nomProfesor")
    private String nomProfesor;

    public Matriculas() {
    }

    public Matriculas(String nroMatricula) {
        this.nroMatricula = nroMatricula;
    }

    public Matriculas(String nroMatricula, String codAlumno, String nomAlumno, String codCurso, String nomProfesor) {
        this.nroMatricula = nroMatricula;
        this.codAlumno = codAlumno;
        this.nomAlumno = nomAlumno;
        this.codCurso = codCurso;
        this.nomProfesor = nomProfesor;
    }

    public String getNroMatricula() {
        return nroMatricula;
    }

    public void setNroMatricula(String nroMatricula) {
        this.nroMatricula = nroMatricula;
    }

    public String getCodAlumno() {
        return codAlumno;
    }

    public void setCodAlumno(String codAlumno) {
        this.codAlumno = codAlumno;
    }

    public String getNomAlumno() {
        return nomAlumno;
    }

    public void setNomAlumno(String nomAlumno) {
        this.nomAlumno = nomAlumno;
    }

    public String getCodCurso() {
        return codCurso;
    }

    public void setCodCurso(String codCurso) {
        this.codCurso = codCurso;
    }

    public String getNomProfesor() {
        return nomProfesor;
    }

    public void setNomProfesor(String nomProfesor) {
        this.nomProfesor = nomProfesor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nroMatricula != null ? nroMatricula.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matriculas)) {
            return false;
        }
        Matriculas other = (Matriculas) object;
        if ((this.nroMatricula == null && other.nroMatricula != null) || (this.nroMatricula != null && !this.nroMatricula.equals(other.nroMatricula))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.persistence.entities.Matriculas[ nroMatricula=" + nroMatricula + " ]";
    }
    
}
