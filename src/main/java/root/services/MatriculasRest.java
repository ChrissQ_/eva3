
package root.services;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.persistence.entities.Matriculas;

/**
 *
 * @author CQuintana
 */

@Path("/matriculas")
public class MatriculasRest {
    
    EntityManagerFactory emFac = Persistence.createEntityManagerFactory("alumnos_PU");
    EntityManager manag;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarAll(){
        manag = emFac.createEntityManager();
        List<Matriculas> listado = manag.createNamedQuery("Matriculas.findAll").getResultList();
        return Response.ok(200).entity(listado).build();
    }
    
    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idbuscar") String idbuscar){
        
        manag = emFac.createEntityManager();
        Matriculas report = manag.find(Matriculas.class, idbuscar);
        return Response.ok(200).entity(report).build();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String nvoAlumno(Matriculas niuAlumno){
        manag = emFac.createEntityManager();
        manag.getTransaction().begin();
        manag.persist(niuAlumno);
        manag.getTransaction().commit();
        return "Alumno ingresado..";
    }
    
    @PUT
    public Response actualizoAlumno(Matriculas updtAlumno){
     
        manag = emFac.createEntityManager();
        manag.getTransaction().begin();
        updtAlumno = manag.merge(updtAlumno);
        manag.getTransaction().commit();
        return Response.ok(updtAlumno).build();
    }
    
    @DELETE
    @Path("/{idAlumno}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
    public Response eliminoMatricula(@PathParam("idAlumno") String idAlumno){
        
        manag = emFac.createEntityManager();
        manag.getTransaction().begin();
        Matriculas chaoMatricula = manag.getReference(Matriculas.class, idAlumno);
        manag.remove(chaoMatricula);
        manag.getTransaction().commit();
        return Response.ok("juiste weno!").build();
    }
}
