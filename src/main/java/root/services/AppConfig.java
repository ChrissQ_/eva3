
package root.services;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author CQuintana
 */

@ApplicationPath("api")
public class AppConfig extends Application{
    
}
